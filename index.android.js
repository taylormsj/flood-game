/**
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { Router } from 'react-native-router-flux';
import { store } from '~/reducers';
import Scenes from '~/scenes';
import { settingsLoadAction } from '~/actions/settings';

const RouterWithRedux = connect()(Router)

export default class FloodGame extends Component {

  componentDidMount() {
    store.dispatch(settingsLoadAction());
  }

  render() {
    return (
      <Provider store={store}>
        <RouterWithRedux>
          <Scenes />
        </RouterWithRedux>
      </Provider>
    );
  }

};

AppRegistry.registerComponent('FloodGame', () => FloodGame);
