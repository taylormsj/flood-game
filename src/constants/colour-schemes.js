// http://stackoverflow.com/questions/34027270/ios-launch-screen-in-react-native
export default {
	// dark: {
	// 	background: '#14233c',
	// 	colours: ['rgb(30,120,140)', 'rgb(154,189,169)', 'rgb(234,209,159)', 'rgb(230,75,95)', 'rgb(0,164,219)', 'rgb(148,155,166)']
	// },
	dark: {
		background: 'rgb(52, 73, 94)',
		lightText: 'rgb(236, 240, 241)',
		darkText: 'rgb(44, 62, 80)',
		colours: ['rgb(46, 204, 113)', 'rgb(52, 152, 219)', 'rgb(155, 89, 182)', 'rgb(241, 196, 15)', 'rgb(230, 126, 34)', 'rgb(231, 76, 60)']
	},
	light: {
		background: 'rgb(236, 240, 241)',
		lightText: 'rgb(52, 73, 94)',
		darkText: 'rgb(236, 240, 241)',
		colours: ['rgb(39, 174, 96)', 'rgb(41, 128, 185)', 'rgb(142, 68, 173)', 'rgb(243, 156, 18)', 'rgb(211, 84, 0)', 'rgb(192, 57, 43)']
	}
};