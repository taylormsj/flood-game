export const GAME_RESET = 'GAME_RESET';
export const GAME_RETRY = 'GAME_RETRY';
export const GAME_COLOUR_CHANGE = 'GAME_COLOUR_CHANGE';
export const GAME_WIN = 'GAME_WIN';

export const SETTINGS_UPDATE_GRID_SIZE = 'SETTINGS_UPDATE_GRID_SIZE';
export const SETTINGS_NEW_HIGH_SCORE = 'SETTINGS_NEW_HIGH_SCORE';
export const SETTINGS_LOAD = 'SETTINGS_LOAD';