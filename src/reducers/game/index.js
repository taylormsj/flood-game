import {
	floodGrid,
	computeMinSteps
} from '../../utils/game';
import * as actionTypes from '../../constants/action-types';

const initialState = {
	grid: null,
	startingGrid: null,
	targetColour: null,
	steps: null,
	minSteps: null,
	win: false,
	gameOver: false,
	inProgress: false
};

const copyArray = original => original.map(arr => arr.slice());

export const gameReducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GAME_RESET: {
			const { grid } = action.payload;
			const startingGrid = copyArray(grid);
			const minSteps = computeMinSteps(copyArray(grid));

			return {
				grid,
				startingGrid,
				targetColour: grid[0][0],
				steps: 0,
				minSteps,
				win: false,
				gameOver: false,
				inprogress: false
			};
		}
		case actionTypes.GAME_RETRY: {
			const grid = copyArray(state.startingGrid);

			return {
				...state,
				grid,
				targetColour: grid[0][0],
				steps: 0,
				win: false,
				gameOver: false,
				inProgress: false
			};
		}
		case actionTypes.GAME_COLOUR_CHANGE: {
			const { replacementColour } = action.payload;
			const result = floodGrid(state.grid, state.targetColour, replacementColour);
			const steps = state.steps + 1;
			const win = result.remainingCells === 0;
			const gameOver = !win && steps === state.minSteps;
			const inProgress = !win && !gameOver;

			return {
				...state,
				grid: result.grid.slice(),
				targetColour: replacementColour,
				steps,
				win,
				gameOver,
				inProgress
			};
		}
	};
	return state;
};