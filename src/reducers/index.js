import {
	createStore,
	combineReducers,
	applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';
import { gameReducer } from './game';
import { settingsReducer } from './settings';

const rootReducer = combineReducers({
	game: gameReducer,
	settings: settingsReducer
});

const logger = (store) => (next) => (action) => {
  console.info(action.type);
  console.info('dispatching', action);
  const result = next(action);
  console.info('next state', store.getState());

  return result;
};

const middleWare = applyMiddleware(thunk, logger);

export const store = createStore(
	rootReducer,
	middleWare
);