import * as actionTypes from '../../constants/action-types';

const initialState = {
	gridSize: 12,
	colourScheme: 'dark',
    highScore: null
};

export const settingsReducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.SETTINGS_LOAD: {
			return {
				...state,
				...action.payload
			};
		}
		case actionTypes.SETTINGS_NEW_HIGH_SCORE: {
			const { highScore } = action.payload;
			return {
				...state,
				highScore
			};
		}
	};
	return state;
};