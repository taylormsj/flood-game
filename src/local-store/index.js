import { AsyncStorage } from 'react-native';

class LocalStore {

	generateKey(key) {
		return `FG|${key}`;
	}

	async setItem(key, value) {
		try {
		  await AsyncStorage.setItem(this.generateKey(key), JSON.stringify(value));
		} catch (error) {
		  console.log('Error setting item', key, value);
		}
	}

	async getItem(key) {
		try {
		  const result = await AsyncStorage.getItem(this.generateKey(key));
		  if (result) {
		  	return JSON.parse(result);
		  }
		  return null;
		} catch (error) {
		  console.log('Error getting item', key);
		}
	}

	async getSettings() {
		try {
		  const result = await AsyncStorage.getItem(this.generateKey('settings'));
		  return JSON.parse(result);
		} catch (error) {
		  console.log('Error getting settings');
		}
	}

	async updateSettings(obj) {
		try {
			let settings = await this.getSettings();
			if (!settings) {
				settings = {};
			}
			const newSettings = {
				...settings,
				...obj
			};
		  return await this.setItem('settings', newSettings);
		} catch (error) {
		  console.log('Error updating settings', obj);
		}
	}

};

export default new LocalStore();