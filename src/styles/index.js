import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  sceneContainer: {
    flex: 1,
    // justifyContent: 'center',
    //alignContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 30
  }
});