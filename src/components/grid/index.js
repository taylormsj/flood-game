import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
  Dimensions
} from 'react-native';
import Cell from '../cell';

const Grid = ({
  grid,
  colours
}) => 
  <View style={styles.grid}>
    {grid.map((row, i) => 
      <View
        key={i}
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'stretch'
        }}
      >
        {row.map((cell, j) => 
          <Cell 
            key={`${i}-${j}`}
            colour={colours[cell]}
          />
        )}
      </View>
    )}
  </View>;

Grid.propTypes = {
  grid: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
  colours: PropTypes.arrayOf(PropTypes.string).isRequired
};

const WIDTH = Dimensions.get('window').width - 40;

const styles = StyleSheet.create({
  grid: {
    width: WIDTH,
    height: WIDTH,
    marginVertical: 20
  }
});

export default Grid;