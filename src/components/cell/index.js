import React, {
  PropTypes,
  PureComponent
} from 'react';
import { Animated } from 'react-native';

// http://browniefed.com/react-native-animation-book/basic/COLOR.html
class Cell extends PureComponent {

  constructor(props) {
    super(props);
    this.animate = this.animate.bind(this);
    this.interpolatedColorAnimation = null;
  }

  componentWillMount() {
    const { colour } = this.props;
    this._animatedValue = new Animated.Value(0);
    this.interpolatedColorAnimation = this._animatedValue.interpolate({
      inputRange: [0, 100],
      outputRange: ['rgba(0, 0, 0,0)', colour]
    });
  }

  componentDidMount() {
    this.animate(500);
  }

  componentWillReceiveProps(nextProps) {
    const { colour } = this.props;
    if (colour !== nextProps.colour) {
      this._animatedValue = new Animated.Value(0);
      this.interpolatedColorAnimation = this._animatedValue.interpolate({
        inputRange: [0, 100],
        outputRange: [colour, nextProps.colour]
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { colour } = this.props;
    if (colour !== prevProps.colour) {
      this.animate();
    }
  }

  animate(duration) {
    Animated.timing(this._animatedValue, {
      toValue: 100,
      duration: duration || 150
    }).start();   
  }

  render() {
    return ( 
      <Animated.View 
        style={{
          flex: 1,
          backgroundColor: this.interpolatedColorAnimation
        }}
      />
    );
  }

}

Cell.propTypes = {
  colour: PropTypes.string.isRequired
};

export default Cell;