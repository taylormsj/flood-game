import React, { PropTypes } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text
} from 'react-native';

const Button = ({
  onPress,
  children,
  backgroundColor,
  textColour
}) => 
  <TouchableOpacity
    onPress={onPress.bind(this)}
    activeOpacity={0.6}
    style={[
      styles.button,
      { backgroundColor }
    ]}
  >
    <Text style={{
      color: textColour,
      fontSize: 14,
      fontFamily: 'AvenirNext-Bold'
    }}>
      {children}
    </Text>
  </TouchableOpacity>;

Button.defaultProps = {
  backgroundColor: 'rgb(236, 240, 241)',
  textColour: 'rgb(44, 62, 80)'
};

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  textColour: PropTypes.string
};

const styles = StyleSheet.create({
  button: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 6,
    marginLeft: 10
  }
});

export default Button;