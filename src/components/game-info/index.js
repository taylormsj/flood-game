import React, { PropTypes } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Button from '../button';

const GameInfo = ({
  steps,
  minSteps,
  bonusTimer,
  colourScheme,
  gameReset,
  gameRetry
 }) => 
  <View style={styles.container}>
    <View style={{ flexDirection: 'column' }}>
      <Text style={[
        styles.text,
        {
          color: bonusTimer > 0.0 ? colourScheme.lightText : 'red'
        }
      ]}>
        BONUS: {parseFloat(bonusTimer).toFixed(2)}
      </Text>
      <Text style={[
        styles.text,
        { color: colourScheme.lightText }
      ]}>
        STEPS: {steps} / {minSteps}
      </Text>
    </View>
    <View style={{ flexDirection: 'row' }}>
      <Button onPress={gameReset.bind(this)} backgroundColor={colourScheme.lightText} textColour={colourScheme.darkText}>
        NEW
      </Button>
      <Button onPress={gameRetry.bind(this)} backgroundColor={colourScheme.lightText} textColour={colourScheme.darkText}>
        RETRY
      </Button>
    </View>
  </View>;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  text: {
    color: 'rgb(236, 240, 241)',
    // fontFamily: 'AvenirNext-Medium',
    fontSize: 14,
    fontFamily: 'AvenirNext-Bold',
  }
});

GameInfo.propTypes = {
  steps: PropTypes.number.isRequired,
  minSteps: PropTypes.number.isRequired,
  bonusTimer: PropTypes.number,
  colourScheme: PropTypes.object.isRequired,
  gameReset: PropTypes.func.isRequired,
  gameRetry: PropTypes.func.isRequired
};

export default GameInfo;