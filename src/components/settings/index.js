import React, { PropTypes } from 'react';
import {
  View,
  Switch
} from 'react-native';

const Settings = ({
  colourScheme,
  changeColourScheme
}) => 
  <View>
    <Switch
      value={colourScheme === 'dark'}
      onValueChange={changeColourScheme.bind(this)}
    />
  </View>;

Settings.propTypes = {
  colourScheme: PropTypes.string.isRequired,
};

export default Settings;