import React, { Component, PropTypes } from 'react';
import { Animated } from 'react-native';
import Title from '../title';

class Results extends Component {

  constructor(props) {
    super(props);
    this.bounceValue = new Animated.Value(0.4);
  }

  componentDidMount() {
    // https://rnplay.org/apps/PD2c1g
    Animated.spring(                        
      this.bounceValue,               
      {
        toValue: 1,  
        friction: 3,
        tension: 40,
      }
    ).start();
  }

  render () {
    const {
      colourScheme,
      result
    } = this.props;

    return (
      <Animated.View
        style={{
          transform: [                     
            { scale: this.bounceValue },
          ]
        }}
      >
        <Title
          colour={colourScheme.lightText}
          style={{
            fontSize: 40,
            marginTop: -10
          }}
        >
          {result}
        </Title>
      </Animated.View>
    );
  }

};

Results.PropTypes = {
  colourScheme: PropTypes.object.isRequired,
  result: PropTypes.string.isRequired
};

export default Results;