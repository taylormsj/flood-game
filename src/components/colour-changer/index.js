import React, { PropTypes } from 'react';
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';

const ColourChanger = ({
  colourScheme,
  targetColour,
  clickHandler,
  bonusTimer
 }) => 
  <View style={styles.container}>
    {colourScheme.colours.map((colour, index) => 
      <TouchableWithoutFeedback
        key={colour}
        onPress={clickHandler.bind(this, index, bonusTimer)}
      >
        <View
          style={[
            styles.button, 
            { 
              backgroundColor: colour,
              borderColor: colourScheme.colours[targetColour] === colour ? colourScheme.background : colour
            }
          ]}
        />
      </TouchableWithoutFeedback>
    )}
  </View>;

ColourChanger.propTypes = {
  colourScheme: PropTypes.object.isRequired,
  targetColour: PropTypes.number.isRequired,
  clickHandler: PropTypes.func.isRequired,
  bonusTimer: PropTypes.number
};

const WIDTH = (Dimensions.get('window').width - 40) / 6 - (8);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    width: WIDTH,
    height: WIDTH,
    borderRadius: WIDTH / 2,
    borderWidth: 2,
    borderStyle: 'solid'
  }
});

export default ColourChanger;