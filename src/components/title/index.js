import React, { PropTypes } from 'react';
import {
  StyleSheet,
  Text
} from 'react-native';

const Title = ({
  colour,
  style,
  children
}) => 
  <Text
    style={[
      styles.title,
      style,
      { color: colour }
    ]}
  >
    {children}
  </Text>

Title.propTypes = {
  colour: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
  title: {
    color: 'white',
    fontFamily: 'AvenirNext-Heavy',
    fontSize: 44,
    textAlign: 'center'
  }
});

export default Title;