import React, {
  Component,
  PropTypes
} from 'react';
import {
  Animated,
  Easing
} from 'react-native';

class ScoreBar extends Component {

  constructor(props) {
    super(props);
    this._animatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    const { score } = this.props;
    Animated.timing(                        
      this._animatedValue,               
      {
        toValue: score, // ToDo - Normalise between 0 - 1
        duration: 300,
        easing: Easing.exp
      }
    ).start();
  }

  render () {
    const {
      colourScheme,
      result
    } = this.props;

    return (
      <View style={{
        flex: 1,
        height: 30
      }}>
        <Animated.View
          style={{
            height: '100%',
            flex: this._animatedValue
          }}
        />
      </View>
    );
  }

};

ScoreBar.PropTypes = {
  score: PropTypes.number.isRequired
};

export default ScoreBar;