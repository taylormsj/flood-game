import React from 'react';
import { connect } from 'react-redux';
import { Scene } from 'react-native-router-flux';
import Main from './main';
import Game from './game';
import Settings from './settings';
import colourSchemes from '../constants/colour-schemes';

const Scenes = ({ colourScheme }) =>
  <Scene
    key='root'
    hideNavBar
    style={{ backgroundColor: colourScheme.background }}
  >
    <Scene key='main' component={Main} />
    <Scene key='game' component={Game} initial />
    <Scene key='settings' component={Settings} />
  </Scene>;

const mapStateToProps = (state) => ({
  colourScheme: colourSchemes[state.settings.colourScheme]
});

export default connect(
  mapStateToProps
)(Scenes);