import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import Title from '../../components/title';
import GameInfo from '../../containers/game-info';
import Grid from '../../containers/grid';
import ColourChanger from '../../containers/colour-changer';
import Results from '../../components/results';
import { gameResetAction } from '../../actions/game';
import colourSchemes from '../../constants/colour-schemes';
import Styles from '../../styles';

const BONUS_TIME = 30;

class Game extends Component {

  timerInterval = null;

  constructor(props) {
    super(props);
    const { dispatchGameReset } = props;
    this.updateTimer = this.updateTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    dispatchGameReset();
    this.state = {
      bonusTimer: BONUS_TIME
    };
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.inProgress && this.props.inProgress === true) {
      this.timerInterval = setInterval(this.updateTimer, 10);
    } else if (prevProps.inProgress === true && !this.props.inProgress) {
      this.resetTimer();
    }
  }

  updateTimer() {
    const newTimer = this.state.bonusTimer - 0.01;
    if (newTimer >= 0.0) {
      this.setState({
        bonusTimer: newTimer
      });
    } else {
      this.resetTimer(0);
    }
  }

  resetTimer(val) {
    clearInterval(this.timerInterval);
    this.setState({
      bonusTimer: (val !== undefined) ? val : BONUS_TIME
    });
  }

  render() {
    const {
      grid,
      colourScheme,
      win,
      gameOver,
      highScore
    } = this.props;
    const { bonusTimer } = this.state;

    if (!grid) {
      return null;
    }

    return (
      <View style={[
        Styles.sceneContainer,
        { backgroundColor: colourScheme.background }
      ]}>
        <Title
          colour={colourScheme.lightText}
          style={{
            marginBottom: 0
          }}
        >
          TILE FLOOD
        </Title>
        <Text style={{
          color: colourScheme.lightText,
          textAlign: 'center',
          marginBottom: 30,
          fontSize: 16,
          fontFamily: 'AvenirNext-Bold',
          opacity: highScore ? 1 : 0
        }}>
          HIGH SCORE: {highScore}
        </Text>
      	<GameInfo bonusTimer={bonusTimer} />
        <Grid />
        {win || gameOver ? (
          <Results
            colourScheme={colourScheme}
            result={win ? 'YOU WIN!' : "GAME OVER!"}
          />
        ) : (
          <ColourChanger bonusTimer={bonusTimer} />
        )}
      </View>
    );
  }

};

const mapStateToProps = (state) => ({
  grid: state.game.grid,
  colourScheme: colourSchemes[state.settings.colourScheme],
  win: state.game.win,
  gameOver: state.game.gameOver,
  inProgress: state.game.inProgress,
  highScore: state.settings.highScore
});

const mapDispatchToProps = (dispatch) => ({
  dispatchGameReset: () => dispatch(gameResetAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);