import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';
import Styles from '../../styles';

class Main extends Component {

  render() {
    return (
      <View style={Styles.sceneContainer}>
        <Text>
          Main
        </Text>
      </View>
    );
  }

};

export default Main;