import { generateGrid } from '../../utils/game';
import * as actionTypes from '../../constants/action-types';
import colourSchemes from '../..//constants/colour-schemes';
import { settingsPersistScoreAction } from '../settings';

export const gameResetAction = () => (dispatch, getState) => {
	const {
		gridSize,
		colourScheme
	} = getState().settings;

	const grid = generateGrid(gridSize, colourSchemes[colourScheme].colours);
	
	dispatch({
    type: actionTypes.GAME_RESET,
    payload: {
    	grid
    }
  });
};

export const gameRetryAction = () => ({
  type: actionTypes.GAME_RETRY,
});

export const gameColourChangeAction = (replacementColour, bonusTimer) => (dispatch, getState) => {
	const {
		targetColour,
		win,
		gameOver
	} = getState().game;
	if (targetColour === replacementColour || win || gameOver) {
		return;
	}

	dispatch({
		type: actionTypes.GAME_COLOUR_CHANGE,
		payload: {
			replacementColour
		}
	});

	const {
		steps,
		minSteps
	} = getState().game;
	const hasWon = getState().game.win;

	if (hasWon) {
		let score = parseInt((minSteps - steps) * 100 + (bonusTimer * 100));
		dispatch(settingsPersistScoreAction(score));
	}
};