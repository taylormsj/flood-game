import * as actionTypes from '../../constants/action-types';
import LocalStore from '../../local-store';

export const settingsLoadAction = () => async (dispatch) => {
	try {
		const settings = await LocalStore.getSettings();
		if (settings) {
			dispatch({
				type: actionTypes.SETTINGS_LOAD,
				payload: settings
			});
		} else {
			console.log('No settings to load');
		}
	} catch (e) {
		console.log('Error loading settings');
	}
}

export const settingsPersistScoreAction = (steps) => (dispatch, getState) => {
	const { highScore } = getState().settings;
	if (!highScore || (highScore && steps > highScore)) {
		dispatch({
			type: actionTypes.SETTINGS_NEW_HIGH_SCORE,
			payload: {
				highScore: steps
			}
		});
		LocalStore.updateSettings({ highScore: steps });
	}
};