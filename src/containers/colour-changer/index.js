import { connect } from 'react-redux';
import ColourChanger from '../../components/colour-changer';
import colourSchemes from '../../constants/colour-schemes';
import { gameColourChangeAction } from '../../actions/game';

const mapStateToProps = (state) => ({
  colourScheme: colourSchemes[state.settings.colourScheme],
  targetColour: state.game.targetColour
});

const mapDispatchToProps = (dispatch) => ({
  clickHandler: (replacementColour, bonusTimer) => dispatch(gameColourChangeAction(replacementColour, bonusTimer))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ColourChanger);