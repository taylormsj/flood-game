import { connect } from 'react-redux';
import Grid from '../../components/grid';
import colourSchemes from '../../constants/colour-schemes';

const mapStateToProps = (state) => ({
  grid: state.game.grid,
  colours: colourSchemes[state.settings.colourScheme].colours
});

export default connect(
  mapStateToProps,
  null
)(Grid);