import { connect } from 'react-redux';
import GameInfo from '../../components/game-info';
import {
  gameResetAction,
  gameRetryAction
} from '../../actions/game';
import colourSchemes from '../../constants/colour-schemes';

const mapStateToProps = (state) => ({
  steps: state.game.steps,
  minSteps: state.game.minSteps,
  colourScheme: colourSchemes[state.settings.colourScheme]
});

const mapDispatchToProps = (dispatch) => ({
  gameReset: () => dispatch(gameResetAction()),
  gameRetry: () => dispatch(gameRetryAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameInfo);