/**
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { store } from '~/reducers';
import { settingsLoadAction } from '~/actions/settings';
import Game from './src/scenes/game';

export default class FloodGame extends Component {

	componentDidMount() {
		store.dispatch(settingsLoadAction());
	}

  render() {
    return (
      <Provider store={store}>
        <Game />
      </Provider>
    );
  }

};

AppRegistry.registerComponent('FloodGame', () => FloodGame);
